// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')
module.exports = {
  build: {
    env: require('./prod.env'),
      index: path.resolve(__dirname, '../../iot/src/main/resources/templates/auth/login.html'),
      assetsRoot: path.resolve(__dirname, '../../iot/src/main/resources/static'),
    assetsSubDirectory: '',
    assetsPublicPath: process.env.npm_config_iot ? '/iot/' : '/',
    productionSourceMap: false,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: require('./dev.env'),
    port: 8002,
    autoOpenBrowser: true,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
   //proxyTable: {},
      proxyTable: {
          "/wechat": {//以/proxy/为开头的适合这个规则
              target:'https://www.shangjistore.com/',
              // target: "http://192.168.0.103/",//目标地址
              "secure": false,//false为http访问，true为https访问
              "changeOrigin": true,//跨域访问设置，true代表跨域
              "pathRewrite": {//路径改写规则
                  "^/wechat": ""//以/proxy/为开头的改写为''
              },
          }

      },
    context: [],//代理路径
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false
  }
}
