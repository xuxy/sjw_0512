const login = r => require.ensure([], () => r(require('@/page/login')), 'login');
const home = r => require.ensure([], () => r(require('@/page/home')), 'home');
const sjdy = r => require.ensure([], () => r(require('@/page/sjdy')), 'sjdy');
const market = r => require.ensure([], () => r(require('@/page/market')), 'market');
const focus = r => require.ensure([], () => r(require('@/page/focus')), 'focus');
const resfre = r => require.ensure([], () => r(require('@/page/resfre')), 'resfre');
const restime = r => require.ensure([], () => r(require('@/page/restime')), 'restime');
const consfre = r => require.ensure([], () => r(require('@/page/consfre')), 'consfre');
const constime = r => require.ensure([], () => r(require('@/page/constime')), 'constime');
const config = r => require.ensure([], () => r(require('@/page/config')), 'config');
const api = r => require.ensure([], () => r(require('@/page/api')), 'api');
const DashBoard = r => require.ensure([], () => r(require('@/components/DashBoard')), 'DashBoard');
const gywm = r => require.ensure([], () => r(require('@/page/gywm')), 'gywm');
const hyzx = r => require.ensure([], () => r(require('@/page/hyzx/hyzx')), 'hyzx');
const index = r => require.ensure([], () => r(require('@/page/index')), 'index');
const mtbd = r => require.ensure([], () => r(require('@/page/mtbd/mtbd')), 'mtbd');
const ymdf = r => require.ensure([], () => r(require('@/page/ymdf')), 'ymdf');
const wdsr = r => require.ensure([], () => r(require('@/page/wdsr')), 'wdsr');
const wddy = r => require.ensure([], () => r(require('@/page/ymdf/wddy')), 'wddy');
const wdsc = r => require.ensure([], () => r(require('@/page/wdsc')), 'wdsc');
const wdyy = r => require.ensure([], () => r(require('@/page/wdyy')), 'wdyy');
const wdfb = r => require.ensure([], () => r(require('@/page/wdfb')), 'wdfb');
const yhxz = r => require.ensure([], () => r(require('@/page/yhxz')), 'yhxz');
const shangji = r => require.ensure([], () => r(require('@/page/shangji/shangji')), 'shangji');


const routes = [
	{
		path: '/',
		component: login
	},
	{
		path: '/index',
		component: index,
		meta:{auth:true},
        redirect:'/shangji',
		children:[{
			path:'/shangji',
            component:shangji,
			meta:{auth:true}
		},
        {
            path:'/mtbd',
            component:mtbd,
            name:'mtbd',
            meta:{auth:true},
        },
        {
            path:'/hyzx',
            component:hyzx,
            name:'hyzx',
            meta:{auth:true},
        },{
			path:'/focus',
			component:focus,
			meta:{auth:true}
		},{
			path:'/resfre',
			component:resfre,
			meta:{auth:true}
		},{
			path:'/restime',
			component:restime,
			meta:{auth:true}
		},{
			path:'/consfre',
			component:consfre,
			meta:{auth:true}
		},{
			path:'/constime',
			component:constime,
			meta:{auth:true}
		},{
			path:'/config',
			component:config,
			meta:{auth:true}
		},]
	},{
        path:'/gywm',
        name:'gywm',
        component:gywm,
        meta:{auth:true}
    },{
        path:'/sjdy',
        component:sjdy,
        meta:{auth:true}
    },



    {
        path:'/index',
        component:index,
        name:'index',
        meta:{auth:true},
    },

    {
        path:'/ymdf',
        component:ymdf,
        name:'ymdf',
        meta:{auth:true},
        redirect:'/wddy',
        children:[{
                path:'/wddy',
                component:wddy,
                name:'wddy',
                meta:{auth:true},
            } ]
    },

    {
        path:'/wdsr',
        component:wdsr,
        name:'wdsr',
        meta:{auth:true},
    },{
        path:'/wdsc',
        component:wdsc,
        name:'wdsc',
        meta:{auth:true},
    },

    {
        path:'/wdyy',
        component:wdyy,
        name:'wdyy',
        meta:{auth:true},
    },{
        path:'/wdfb',
        component:wdfb,
        name:'wdfb',
        meta:{auth:true},
    },

    {
        path:'/yhxz',
        component:yhxz,
        name:'yhxz',
        meta:{auth:true},
    },

    {
        path:'/api',
        component:api,
        name:'api_page',
        meta:{auth:true},
    }
]
export default routes
