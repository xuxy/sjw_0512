/**
 * 公共
 */
export const HELPICON = {
  ICON:"question-circle",
  ICONCOLOR:"#0086d0"
}
export const TIMEZONES = [
	{name:"today",value:1},
	{name:"30day",value:30},
	{name:"90day",value:90},
	{name:"180day",value:180},
]
/**
 * 银行图标
 */
export { BANKICON } from "./bank"
/**
 * 图表配置
 */
export { COLORS,GETSERIESDATA,GETLEGENDDATA,PIESERIESDATA,CHINAMAPOPTION,LINEARGRADIENTCOLORS,LINEOPTION } from "./charts"
/**
 * 区域市场分析
 */
// export * from "./meta/market"
export { ANALYSISTYPE,ANALYSISTITLES,ACTTITLES,SHARETITLES,CHINAMAPTITLES,TABLEHEAD } from "./meta/market"
/*
* 活跃质量聚焦分析
* */
export { TYPEANATITLES,TYPETRENDTITLES,FREANATITLES,FREQUENTTITLES } from "./meta/focus"
/*
* 营销响应度分析
* */
export { REFRETYPES,REFREANALYSISTYPES,RESFRE_LEFT_1_TITLES,RESFRE_LEFT_2_TITLES,RESFRE_LEFT_3_TITLES,RESFRE_RIGHT_1_TITLES,RESFRE_RIGHT_2_TITLES,RESFRE_RIGHT_3_HEAD } from "./meta/resfre"
/*
* 营销响应时间段分析
* */
export { TIMEANATITLES,TIMEFRETITLES,COUNTANATITLES } from "./meta/restime"
/*
* 消费频率分析
* */
export { CONSFRETYPES,CONSFREANALYSISTYPES,CONSFRE_LEFT_1_TITLES,CONSFRE_LEFT_2_TITLES,CONSFRE_LEFT_3_TITLES,CONSFRE_RIGHT_1_TITLES,CONSFRE_RIGHT_2_TITLES,CONSFRE_RIGHT_3_HEAD } from "./meta/consfre"

/*
* 消费时段分析
* */

export { CONSTIMETITLES,CONSRATETITLES,CONSCOUNTTITLES } from "./meta/constime"



//本行消费人数分析
export const APILINETITLES = {
    HEADTITLE:"API调用情况",
    CONTENT:`
        只提供本行API调用情况`,
}

