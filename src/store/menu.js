export const MENU = [
	{
		title:"区域市场分析",
		path:"/market",
		icon:"balance-scale"
	},
	{
		title:"活跃质量聚焦分析",
		path:"/focus",
		icon:"money-check"
	},
	{
		title:"营销响应度分析",
		path:"/resfre",
		icon:"money-check"
	},
	{
		title:"营销响应时间段分析",
		path:"/restime",
		icon:"clock"
	},
	{
		title:"消费频率分析",
		path:"/consfre",
		icon:"money-check"
	},
	{
		title:"消费时段分析",
		path:"/constime",
		icon:"money-check"
	},
	// {
	// 	title:"基础配置",
	// 	path:"/config",
	// 	icon:"cog"
	// },
]