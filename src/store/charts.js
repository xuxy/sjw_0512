/**
 * [COLORS description]
 * @type {Array}
 */
export const COLORS = ['#45a6db', '#78e1e5','#c5df88', '#feb580','#fe8781','#f00']
/**
 * [圆环图获取series数据]
 * @param  {[object]} data [{name:"",value:""}]
 * @return {[type]}      [description]
 */
export const GETSERIESDATA = (data) => {
	const seriesData = []
	if(typeof(data) == "object") {
		const maxAngel = 97, width = 15, itemGap = 5
		data.map((v,i) => {
			let endAngel = maxAngel - width*i - itemGap*i
			let startAngel = endAngel - width
			seriesData.push({
                type:'pie',
                clockWise:false,
                center: ['40%', '50%'],
                radius : [startAngel+"%",endAngel+"%"],
                itemStyle : {
      			        normal: {
      			            label: {
                          position: 'inner',
                          formatter: '{d}%'
                          // formatter: function(params){
                          //   if(params.name){
                          //     return `{a|${params.percent}%}`
                          //   }
                          // }
      			            }
      			        }
      			    },
                hoverAnimation: false,
                avoidLabelOverlap:true,
                data:[v,{
                        value: 1 - v.value,
                        itemStyle: {
                            normal: {
                                color: 'transparent',
                            }
                        }
                    }
                ]
			})
		})
	}
	return seriesData;
}
/**
 * [圆环图获取legend数据]
 * @param  {[object]} data [{name:"",value:""}]
 * @return {[type]}      [description]
 */
export const GETLEGENDDATA = (data) => {
	const legendData = []
    data.map((v,i) => {
        legendData.push({
            name:data[i].name,
            icon:"none",
            textStyle:{
	            fontSize:12,
	            padding:[3,10],
	            lineHeight:16,
	            color:'#fff',
	            backgroundColor:COLORS[i],
	            borderRadius: 4,
              width:100,
              rich:{}
            }
        })
    })
    return {
        itemGap:10,
        orient: 'vertical',
        right: 5,
        top:"center",
        data:legendData,
    }
}
/**
 * [饼图获取series数据]
 * @param  {[type]} data [{name:"",value:""}]
 * @return {[type]}      [description]
 */
export const PIESERIESDATA = (data) => {
	const seriesData = [{
        type: 'pie',
        radius : '90%',
        center: ['40%', '50%'],
        data:data,
        label: {
            normal: {
                position: 'inner',
                formatter: '{d}%',
                textStyle: {
                    color: '#fff',
                    fontWeight: 'bold',
                    fontSize: 12
                }
            }
        },
        itemStyle: {
            emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    }]
	return seriesData;
}
/**
 * [china地图配置]
 * @type {Object}
 */
export const CHINAMAPOPTION = {
  grid:{
      top:'0',
      containLabel: true
  },
  visualMap: {
      min: 0,
      max: 100,
      left: 50,
      top: 'bottom',
      color: ['#c05050','#e5cf0d','#5ab1ef'],
      calculable: true,
      itemWidth:10,
      itemHeight:100
  },
  geo: {
      map: 'china',
      roam:false,
      label: {
          emphasis: {
              show: true,
              textStyle:{
                  color:"#fff",
                  fontSize:16,
              },
          }
      },
      itemStyle: {
          normal: {
              areaColor: '#00a5d7',
              borderColor: '#fff'
          },
          emphasis: {
              areaColor: '#175ce7',
          }
      }
  },
  series:[{
      type: 'map',
      mapType: 'china',
      geoIndex: 0,
      label: {
          normal: {
              show: true
          },
          emphasis: {
              show: true
          }
      },
      data: []
  }]
}
/**
 * [渐变色]
 * @type {Array}
 */
export const LINEARGRADIENTCOLORS = [{
    areaStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(252,91,83, 0.1)'
        }, {
          offset: 0.8,
          color: 'rgba(252,91,83, 0)'
        }], false),
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowBlur: 10
      }
    },
    itemStyle: {
      normal: {
        color: 'rgb(252,91,83)'
      }
    }
  },{
    areaStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(0,133,208, 0.1)'
        }, {
          offset: 0.8,
          color: 'rgba(0,133,208, 0)'
        }], false),
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowBlur: 10
      }
    },
    itemStyle: {
      normal: {
          color: 'rgb(0,133,208)'
      }
    }
  }
]
/**
 * [渐变折线图配置]
 * @param  {[type]} data [类目数量]
 * @return {[type]}      [description]
 */
export const LINEOPTION = (data) => {
  const seriesData = []
  for(let i = 0;i < data;i++){
    seriesData.push(_.merge(LINEARGRADIENTCOLORS[i],{
      type: 'line',
      smooth: true,
      showSymbol:false,
      lineStyle: {
          normal: {
              width: 1
          }
      },
      markPoint: {
        symbolSize:45,
        data: [
            {type: 'max', name: '最大值'},
            {type: 'min', name: '最小值'},
        ]
      }
    }))
  }
  const option = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            lineStyle: {
                color: '#57617B'
            }
        }
    },
    legend: {
        icon: 'circle',
        left: 'center',
        top: 0,
        textStyle: {
            fontSize: 12,
            color: '#686868'
        }
    },
    grid: {
        left: 10,
        right: 40,
        bottom: 0,
        top:20,
        containLabel: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        axisLine: {
            show:false,
            lineStyle: {
                color: '#57617B'
            }
        },
        axisTick: {
            show: false
        }
    },
    yAxis: [{
        type: 'value',
        boundaryGap: [0, 1.2],
        axisTick: {
            show: false
        },
        axisLine: {
            show:false,
            lineStyle: {
                color: '#57617B'
            }
        },
        axisLabel: {
            margin: 10,
            textStyle: {
                fontSize: 10
            }
        },
        splitLine: {
            show:false
        }
    }],
    series:seriesData
  }
  return option;
}
