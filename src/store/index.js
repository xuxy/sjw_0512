import Vue from 'vue'
import Vuex from 'vuex'
import {setStore,getStore} from '@/controller/utils'
Vue.use(Vuex)

const state = {
	isLogin:true,
	userInfo:{},
	headTimeType:0,
	headTimeTypeName:"当日",
    // area:'311',//头部所选区域ID
    // bank:101001,//头部所选银行ID
    areaName:null,//头部所选区域名称
    headselectTime:'20181002',//头部选择时间
    area:null,//头部所选区域ID
    bank:null,//头部所选银行ID
}

const mutations = {
	changeLoginStatus(state, isLogin){
		state.isLogin = isLogin;
	}
}

const actions = {
	// async getUserList(){
	// 	try{
	// 		await user({}).then(res => {
 //    			console.log(res)
 //    		}).catch(err => {
 //    			console.log(err)
 //    		})
	// 	}catch(err){
	// 		console.log(err)
	// 	}
	// }
}

for(var item in state){
    getStore(item)?state[item] = JSON.parse(getStore(item)): false;
}
export default new Vuex.Store({
	state,
	actions,
	mutations,
})
