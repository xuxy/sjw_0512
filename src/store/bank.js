export const BANKICON = {
    "河北银行": "l-hebeiyinhang",
    "招商银行": "l-zhaoshangyinhang",
    "苏州银行": "l-suzhouyinhang",
    "上海银行": "l-shanghaiyinhang",
    "重庆银行": "l-chongqingyinhang",
    "包商银行": "l-baoshangyinhang",
    "建设银行": "l-jiansheyinhang",
    "中信银行": "l-zhongxinyinhang",
    "光大银行": "l-guangdayinhang",
    "农业银行": "l-nongyeyinhang",
    "徽商银行": "l-huishangyinhang",
    "上海浦东发展银行": "l-pufayinhang",
    "盛京银行": "l-shengjingyinhang",
    "吉林银行": "l-jilinyinhang",
    "北京银行": "l-beijingyinhang",
    "渤海银行": "l-bohaiyinhang",
    "成都银行": "l-chengduyinhang",
    "大连银行": "l-dalianyinhang",
    "广发银行": "l-guangfayinhang",
    "杭州联合银行": "l-hangzhoulianheyinhang",
    "赣州银行": "l-ganzhouyinhang",
    "华夏银行": "l-huaxiayinhang",
    "嘉兴银行": "l-jiaxingyinhang",
    "杭州银行": "l-hangzhouyinhang",
    "南京银行": "l-nanjingyinhang",
    "青岛银行": "l-qingdaoyinhang",
    "深圳发展银行": "l-shenzhenfazhanyinhang",
    "台州银行": "l-taizhouyinhang",
    "中国邮政储蓄银行": "l-zhongguoyouzheng",
    "中国银行": "l-zhongguoyinhang",
    "兴业银行": "l-xingyeyinhang",
    "民生银行": "l-minshengyinhang",
    "天津银行": "l-tianjinyinhang",
    "哈尔滨银行": "l-haerbinyinhang",
    "江苏银行": "l-jiangsuyinhang",
    "晋商银行": "l-jinshangyinhang",
    "交通银行": "l-jiaotongyinhang",
    "工商银行": "l-gongshangyinhang",
    "东亚银行": "l-dongyayinhang",
    "中国银联": "l-zhongguoyinlian",
}