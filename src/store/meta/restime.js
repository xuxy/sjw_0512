/*
* 营销响应时间段分析
* */
//timeAna
export const TIMEANATITLES = {
    HEADTITLE:"本行营销响应时段分析",
    CONTENT:`
        营销信息数：银行发送营销短信总数<br/>
        营销信息响应度：响应营销短信条数/发送营销短信条数<br/>
        营销信息占比：营销短信条数/所有短信（营销+消费且符合短信合并规则）条数<br/>
        营销客户人数：与银行之间有营销短信交互的用户总数<br/>
        客户响应度：响应银行之间有营销短信交互的用户总数/与银行之间有营销短信交互的用户总数<br/>
        营销客户占比：接收营销短信条人数/所有接收短信人数<br/>`,
}

//timefrequent
export const TIMEFRETITLES = {
    HEADTITLE:"本行营销信息响应度分析",
    CONTENT:`
        某区域不同时间段内本行营销信息响应度趋势展示。`,
}

//countAna
export const COUNTANATITLES = {
    HEADTITLE:"本行营销信息人数分析",
    CONTENT:`
        某区域不同时间段内本行营销人数度趋势展示。`,
}


