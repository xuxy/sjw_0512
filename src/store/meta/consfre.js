/**
 * 营销响应度分析
 */
export const CONSFRETYPES = ["信息数","客户数","消费率"]
export const CONSFREANALYSISTYPES = ["消费信息数","消费信息占比","消费客户数","消费客户占比"]
// left_1
export const CONSFRE_LEFT_1_TITLES = {
  HEADTITLE:"本行消费频率分析",
  CONTENT:``
}
// left_2
export const CONSFRE_LEFT_2_TITLES = {
	HEADTITLE:"TOP5银行消费信息占比",
	CONTENT:"根据选择区域内发送营销短信总数排名前5的银行"
}
// left_3
export const CONSFRE_LEFT_3_TITLES = {
	HEADTITLE:"TOP5银行消费活跃人数",
	CONTENT:"根据选择区域内响应银行之间有营销短信交互的用户总数/与银行之间有营销短信交互的用户总数数据排名前5的银行"
}
// right_1
export const CONSFRE_RIGHT_1_TITLES = {
	HEADTITLE:"本行消费频率概况"
}
// right_2
export const CONSFRE_RIGHT_2_TITLES = {
	HEADTITLE:"本行消费分析",
	CONTENT:"支持选择营销客户数或是营销信息数单独展示"
}
// right_3
export const CONSFRE_RIGHT_3_HEAD = {
  FIRST:["银行分类","银行名称"],
  SECOND:["当日数据","近30日数据","近90日数据","近180日数据"],
  THIRD:CONSFRETYPES
}
