/*
* 活跃质量聚焦分析
* */
//typeAna
export const TYPEANATITLES = {
    HEADTITLE:"类型分析",
    CONTENT:`主卡：活跃频率最高用户合计<br/>
        副卡一：活跃频率次高用户合计<br/>
        副卡二：活跃频率第三用户合计<br/>
        其他：活跃频率不在前三的用户合计<br/>`
}

//typeTrend
export const TYPETRENDTITLES = {
    HEADTITLE:"类型走势",
    CONTENT:`
        主卡：活跃频率最高用户合计<br/>
      副卡一：活跃频率次高用户合计<br/>
      副卡二：活跃频率第三用户合计<br/>
      其他：活跃频率不在前三的用户合计<br/>`,
}

//frequentAna
export const FREANATITLES = {
    HEADTITLE:"频度分析",
    CONTENT:`活跃：客户通信活跃日均10次以上<br/>
        一般：客户通信活跃日均6-10次<br/>
        偶尔：客户通信活跃日均3-5次<br/>
        沉默：客户通信活跃日均0-2次<br/>`
}

//frequentTrend
export const FREQUENTTITLES = {
    HEADTITLE:"频度走势",
    CONTENT:`
        活跃：客户通信活跃日均10次以上<br/>
        一般：客户通信活跃日均6-10次<br/>
        偶尔：客户通信活跃日均3-5次<br/>
        沉默：客户通信活跃日均0-2次<br/>`,
}
