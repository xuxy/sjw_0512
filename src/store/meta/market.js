/**
 * 区域市场分析
 */
export const ANALYSISTYPE = ["活跃量","份额","环比"]
// left_1
export const ANALYSISTITLES = {
  HEADTITLE:"本行区域市场分析",
  CONTENT:[{
    META:`活跃量：与银行之间有短信交互的用户总数<br/>
      份额：区域内银行累计用户数/区域内所有银行用户数<br/>
      环比：当日的用户量与昨天用户量对比`
    },{
      META:`活跃量：与银行之间有短信交互的用户30日累计总数<br/>
        份额：区域内银行累计用户数/区域内所有银行累计用户数<br/>
        环比：30天的用户量与上一个30天的用户量对比`
    },{
      META:`活跃量：与银行之间有短信交互的用户90日累计总数<br/>
        份额：区域内银行累计用户数/区域内所有银行累计用户数<br/>
        环比：90天的用户量与上一个90天的用户量对比`
    },{
      META:`活跃量：与银行之间有短信交互的用户180日累计总数<br/>
        份额：区域内银行累计用户数/区域内所有银行累计用户数<br/>
        环比：180天的用户量与上一个180天的用户量对比`
    },
  ]
}
// left_2
export const ACTTITLES = {
  HEADTITLE:"TOP5银行区域市场活跃量",
  CONTENT:"根据选择区域内银行之间有短信交互的用户总数排名前5的银行",
}
// left_3
export const SHARETITLES = {
  HEADTITLE:"TOP5银行区域市场份额",
  CONTENT:"根据选择区域内银行累计用户数/区域内所有银行用户数得出的百分比数据排名前5的银行",
}
// right_1
export const CHINAMAPTITLES = {
  HEADTITLE:"本行区域市场活跃量占市场的份额",
}
// right_2
export const TABLEHEAD = {
  FIRST:["银行分类","银行名称"],
  SECOND:["当日数据","近30日数据","近90日数据","近180日数据"],
  THIRD:ANALYSISTYPE
}