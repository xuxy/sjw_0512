/**
 * 营销响应度分析
 */
export const REFRETYPES = ["营销数","响应度","营销占比"]
export const REFREANALYSISTYPES = ["营销信息数","营销信息响应数","营销信息响应度","营销信息占比","营销客户人数","营销响应人数","客户响应度","营销客户占比"]
// left_1
export const RESFRE_LEFT_1_TITLES = {
  HEADTITLE:"本行营销响应度分析",
  CONTENT:`营销信息数：银行发送营销短信总数<br/>
	营销信息响应数：响应营销短信条数<br/>
	营销信息响应度：响应营销短信条数/发送营销短信条数<br/>
	营销信息占比：营销短信条数/所有短信（营销+消费且符合短信合并规则）条数<br/>
	营销客户人数：与银行之间有营销短信交互的用户总数<br/>
	营销响应人数：响应银行之间有营销短信交互的用户总数<br/>
	客户响应度：响应银行之间有营销短信交互的用户总数/与银行之间有营销短信交互的用户总数<br/>
	营销客户占比：接收营销短信条人数/所有接收短信人数
	`
}
// left_2
export const RESFRE_LEFT_2_TITLES = {
	HEADTITLE:"TOP5银行营销信息占比",
	CONTENT:"根据选择区域内发送营销短信总数排名前5的银行"
}
// left_3
export const RESFRE_LEFT_3_TITLES = {
	HEADTITLE:"TOP5银行客户响应占比",
	CONTENT:"根据选择区域内响应银行之间有营销短信交互的用户总数/与银行之间有营销短信交互的用户总数数据排名前5的银行"
}
// right_1
export const RESFRE_RIGHT_1_TITLES = {
	HEADTITLE:"本行营销信息响应度概况"
}
// right_2
export const RESFRE_RIGHT_2_TITLES = {
	HEADTITLE:"本行营销信息分析",
	CONTENT:"支持选择营销客户数或是营销信息数单独展示"
}
// right_3
export const RESFRE_RIGHT_3_HEAD = {
  FIRST:["银行分类","银行名称"],
  SECOND:["当日数据","近30日数据","近90日数据","近180日数据"],
  THIRD:REFRETYPES
}
