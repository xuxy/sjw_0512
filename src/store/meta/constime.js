/*
* 消费时段分析
* */
//本行消费时段分析
export const CONSTIMETITLES = {
    HEADTITLE:"本行消费时段分析",
    CONTENT:`
        消费客户总数：选择时间段内的发送消费信息总人数<br/>
        消费信息总数：选择时间段内的发送消费信息总数。<br/>
        客户占比：消费短信人数/所有收到本行短信人数<br/>
        消费率：消费信息占比：消费短信条数/所有短信条数；`,
}

//本行消费时段分析
export const CONSRATETITLES = {
    HEADTITLE:"本行消费率分析",
    CONTENT:`
        某区域不同时间段内本行消费信息数趋势展示`,
}

//本行消费人数分析
export const CONSCOUNTTITLES = {
    HEADTITLE:"本行消费人数分析",
    CONTENT:`
        某区域不同时间区间内本行消费客户数趋势展示`,
}
