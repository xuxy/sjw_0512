import { baseUrl } from './env'
import axios from 'axios'
axios.defaults.baseURL = baseUrl;

import Qs from 'qs'

export default async(url , data , type = 'GET',) => {
    type = type.toUpperCase()
    url = baseUrl + url

    switch (type) {
        case "GET":
            return axios.get(url,{

                params:data
            })
            break;
        case "LOGIN":
            return axios.post(url,Qs.stringify(data),{
                headers:{
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            })
            break;
        case "POST":
            console.log(url)
            console.log(data)
            return axios.post(url,data)
            break;
        default:
            // statements_def
            break;
    }
}
