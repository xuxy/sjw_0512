import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueJsonp from 'vue-jsonp'
// 引入插件
import './plugins'

Vue.config.productionTip = false;

new Vue({
	el: '#app',
	router,
	store,
    VueJsonp,
	template: '<App/>',
	components: { App }
})
