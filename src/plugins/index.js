// element-ui引入组件
import Vue from 'vue'
import ElementUI from './element-ui'
Vue.use(ElementUI)
// 引入fontawesome图标🆒
import FontAwesomeIcon from './fontawesome'
Vue.component('icon', FontAwesomeIcon)
// 引入IconFont图标🆒
import '../../static/js/iconfont'
import SvgIcon from '@/components/home/_svg_icon.vue';
import '../../static/laydate/laydate/laydate.js'
Vue.component('SvgIcon', SvgIcon);
// lodash函数式库
import _ from 'lodash'
