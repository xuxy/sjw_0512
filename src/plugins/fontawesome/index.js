import Vue from 'vue'
// fontawesome图标🆒全局植入
import { library } from '@fortawesome/fontawesome-svg-core'
// https://fontawesome.com/icons?d=gallery&s=solid

import { faBalanceScale,faCog,faMoneyCheck,faClock,faQuestionCircle,faSignOutAlt,faUser,faCommentDots } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faBalanceScale,faCog,faMoneyCheck,faClock,faQuestionCircle,faSignOutAlt,faUser,faCommentDots)

export default FontAwesomeIcon
