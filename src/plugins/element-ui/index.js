import Vue from 'vue'

import { Message,Notification,Button,Form,FormItem,Input,Card,Container,
    Header,Footer,Aside,Main,Menu,MenuItem,Row,Col,Tooltip,Select,Option,
    DatePicker,RadioGroup,RadioButton,Table,TableColumn,
    Pagination,Carousel, CarouselItem, Dropdown,Checkbox,CheckboxGroup,CheckboxButton,Dialog,Switch,Popover,Divider,Cascader,Image,Radio,
    DropdownMenu, DropdownItem,} from 'element-ui';

export default {
	install (Vue){
		Vue.use(Cascader)
        Vue.use(Radio)
        Vue.use(Image)
        Vue.use(Button)
        Vue.use(Divider)
        Vue.use(Popover)
        Vue.use(Switch)
        Vue.use(Dialog)
		Vue.use(Form)
		Vue.use(FormItem)
		Vue.use(Input)
		Vue.use(Checkbox)
        Vue.use(Checkbox)
        Vue.use(CheckboxGroup)
        Vue.use(CheckboxButton)
        Vue.use(Card)
		Vue.use(Container)
		Vue.use(Header)
		Vue.use(Footer)
		Vue.use(Aside)
		Vue.use(Main)
		Vue.use(Menu)
		Vue.use(MenuItem)
		Vue.use(Row)
		Vue.use(Col)

		Vue.use(Tooltip)
		Vue.use(Select)
		Vue.use(Option)
		Vue.use(DatePicker)
		Vue.use(RadioGroup)
		Vue.use(RadioButton)
		Vue.prototype.$ELEMENT = { size: 'small'};
        Vue.use(Table);
        Vue.use(TableColumn);
        Vue.use(Pagination);
        Vue.use(Carousel);
        Vue.use(CarouselItem);
        Vue.use(Dropdown);
        Vue.use(DropdownMenu);
        Vue.use(DropdownItem);

		Vue.prototype.$message = Message
		Vue.prototype.$notify = Notification
	}
}
