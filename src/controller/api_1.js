import L from '@/config/axios'
/**
 * [查询账号下的银行列表]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryBankByUserId = data => L('/zqjr/areaMarketAnalysis/queryBankByUserId',data,'POST')
/**
 * [查询账号下的区域列表]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryCityByUserId = data => L('/zqjr/areaMarketAnalysis/queryCityByUserId',data,'POST')
/**
 * [查询本行区域市场分析]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryActiveData = data => L('/zqjr/areaMarketAnalysis/queryActiveData',data,'POST')
/**
 * [区域市场分析-本行区域市场分析列表]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryActiveDataAll = data => L('/zqjr/areaMarketAnalysis/queryActiveDataAll',data,'POST')
/**
 * [TOP5银行区域市场活跃量]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryActiveTop5 = data => L('/zqjr/areaMarketAnalysis/queryActiveTop5',data,'POST')
/**
 * [营销响应度分析-本行营销响应度分析]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryMarketingRespons = data => L('/zqjr/marketingResponsAnalysis/queryMarketingRespons',data,'POST')
/**
 * [营销响应度分析-TOP5银行营销信息占比]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryMarketingtop5 = data => L('/zqjr/marketingResponsAnalysis/queryMarketingtop5',data,'POST')
/**
 * [营销响应度分析-本行营销信息分析 走势图]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryMarketing = data => L('/zqjr/marketingResponsAnalysis/queryMarketing',data,'POST')
/**
 * [营销响应度分析-本行营销信息分析]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryMarketingAll = data => L('/zqjr/marketingResponsAnalysis/queryMarketingAll',data,'POST')
/**
 * [消费频率分析-本行消费频率分析]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryConsumptionFrequency = data => L('/zqjr/consumptionFrequencyAnalysis/queryConsumptionFrequency',data,'POST')
/**
 * [消费频率分析-消费频率分析列表]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryConsumptionFrequencyAll = data => L('/zqjr/consumptionFrequencyAnalysis/queryConsumptionFrequencyAll',data,'POST')
/**
 * [消费频率分析-TOP5银行消费活跃人数]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryActiveNumberTOP5 = data => L('/zqjr/consumptionFrequencyAnalysis/queryActiveNumberTOP5',data,'POST')
/**
 * [消费频率分析-TOP5银行消费信息占比]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const queryConsumptionRatioTOP5 = data => L('/zqjr/consumptionFrequencyAnalysis/queryConsumptionRatioTOP5',data,'POST')