import L from '@/config/axios'

export const login = data => L('http://localhost:8080/login', data, "LOGIN");


export const selectAllData = data => L('/businessOpportunity/list?type=3&phoneType=PC&openid=o-a615MVyJKQ12O4Q42JIOmmjlgQ&globalMessage=null',data)

export const selectAllProvinceData = data => L('/common/province',data)

export const selectAllCityData = data => L('/common/province',data)


export const selectAllIndustryType = data => L('/common/sjService',data)

export const affirmPublich = data => L('/businessOpportunity/add',data)


export const  myPublish = data => L('/businessOpportunity/myPublish',data)

export const  updatePublish = data => L('/businessOpportunity/update',data)

export const  sjDetails = data => L('/businessOpportunity/sjDetails',data)

export const  deletePublish = data => L('/businessOpportunity/under',data)

export const  mySubcribe = data => L('/subscribe/mySubcribe',data)

export const  incomeList = data => L('/income/list',data)

export const  noInvoiceList = data => L('/express/noInvoiceList',data)
export const  expressOne = data => L('/express/one',data)
export const  cashList = data => L('/cash/list',data)
export const  expressUpdate = data => L('/express/update',data)

export const  codeWxUnifiedorder = data => L('/wxPay/codeWxUnifiedorder',data)

export const  userSencendRegister = data => L('/user/userSencendRegister',data)

export const  incomeMoney = data => L('/income/money',data)

export const  subscribeList = data => L('/subscribe/list',data)

export const  userInvitedUser = data => L('/user/invitedUser',data)

export const  cashAdd = data => L('/cash/add',data)




